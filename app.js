const express = require('express');
const app = express();
const port = 3000;

const id = parseInt(Math.random() * 10000000);

app.get('/', (req, res) => res.send('Hello World | app_id: ' + id + '<br>'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))